import java.io.File;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author Logan
 *
 */
public class BinaryTreeShape {

	public static final String delim = "\t"; 
	public Node root;
	public BinaryTreeShape(){
		this.root = null; 
	}
	/************************************************************
	 * Read from a file  
	 * Rectangle \t side 1 \t side 2
	 * Circle \t radius
	 * Right Triangle \t side 1 \t side 2
	 ************************************************************/
	//read from file 
	public void fileIn(String fileName){ 
		String split[] = new String[3];
		try{
			Scanner fileScanner = new Scanner(new File(fileName));
			while (fileScanner.hasNextLine()){
				String fileLine = fileScanner.nextLine();	
				String tmp[] = fileLine.split(delim);
				if(tmp.length < 2){ //file line not long enough  
					System.out.println("Invalid Input From File: \"" +tmp[0].toString()+ "\" (Skipped)\n");
					return;
				}
				/*
				 * This is the worst way i could do this but the issue i ran into was 
				 * that when reading the circle line it would not assign split[2] to anyting 
				 * because the fileLine splits based on the delim, but only has one so it 
				 * only has an array of size 2 not 3 like rect and rtiangle. to get around it 
				 * i assigned the input to a tmp and then copied the values to a set size of 3 split[]
				 * then when its last value is null for the circle it sets to 0.0  
				 */
				for(int i=0; i<=3;i++){
					split[0] = tmp[0];
					split[1] = tmp[1];
					if(tmp.length == 2){
						split[2] = "0.0";
					}else 
						split[2] = tmp[2];
				}
				String shape = split[0].trim(); //name of shape: circle, rectangle, right triangle
//				System.out.println(split[0] +" = "+ shape); //error check 
				double sideOne = Double.parseDouble(split[1]); //assign to variable sideOne
//				System.out.println(split[1] +" = "+ sideOne); //error check
				double sideTwo = Double.parseDouble(split[2]); //assign to variable sideTow
//				System.out.println(split[2] +" = "+ sideTwo); //error check  
//				System.out.println(shape+"\t"+sideOne+"\t"+sideTwo);
				this.setShape(shape, sideOne, sideTwo); //call set shape passing in values from file 
			}//end while loop
			fileScanner.close();
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
	}
	private void setShape(String shape,double sideOne, double sideTwo){
		Node newNode = new Node();
		/***************************************************************
		 * create a new shape within each node.data
		 * 
		 * if a circle, set the radius, 
		 * 		calculate perimeter(circumferance), and area
		 * else if a rectangle, set length and width 
		 * 		calculate perimeter, and area
		 * else if a right triangle, set base and height
		 * 		calculate perimeter, and area
		 * 
		 * set each of the instances to the newNode.data created above
		 ****************************************************************/
		if(shape.equalsIgnoreCase("Circle")){
			Circle c = new Circle();
			c.setRadii(sideOne);
			newNode.data = c;
			/*System.out.println(shape+" area"+ newNode.data.getArea()+"\tPerimeter: "+ newNode.data.getPerimeter()
								+shape+"\t" + sideOne);*/
		}else if(shape.equalsIgnoreCase("Rectangle")){
			Rectangle r = new Rectangle();
			r.setLength(sideOne);
			r.setWidth(sideTwo);
			newNode.data = r;
			/*System.out.println(shape+" area: "+ newNode.data.getArea()+"\tPerimeter: "+ newNode.data.getPerimeter()
								+"\t" + sideOne +"\t" + sideTwo);*/
		}else if(shape.equalsIgnoreCase("Right Triangle")){
			RightTriangle rt = new RightTriangle();
			rt.setBase(sideOne);
			rt.setHeight(sideTwo);
			newNode.data = rt;
			/*System.out.println(shape+" area: "+ newNode.data.getArea()+"\tPerimeter: "+ newNode.data.getPerimeter()
								+shape+"\t" + sideOne +"\t" + sideTwo);*/
		}
		insert(newNode); //call insert after creating new node
	}
	//insert, does not need to be public it reads from file first 
	private void insert(Node newNode){
		if(root == null){//empty tree
			root = newNode; //place at root 
		}else{
			toInsert(root,newNode);
		}		
	}
	//insert helper method 
	private Node toInsert(Node currNode, Node newNode) {
		if(currNode == null)//if found spot and is empty place 
			currNode = newNode;
		else if(newNode.data.getArea() < currNode.data.getArea()) //go left, node to add's area < current node's area
			currNode.left = toInsert(currNode.left,newNode);
		else if(newNode.data.getArea() >= currNode.data.getArea()) //go right, node to add's area >= current node's area
			currNode.right = toInsert(currNode.right,newNode);
		return currNode; 
	}
	/*****************************************************************************
	 * Deletion, one with matching area and type && all with greater than  area  
	 * not the best way to code this but keep the user from having to pass in 
	 * unnecessary data, just input the type of shape, area and the 
	 * code, k 1 = only one instance of shape&area  
	 * 2 = all instances of all shapes with area greater than value passed in  
	 ****************************************************************************/
	//delete one node with passed in area
	public void delete(String type, double area){
		toDelete(type, root, area);
	}
	//deletion helper method 
	private Node toDelete(String type, Node aNode, double value){ 
		if(aNode == null) return null;
		else if(value < aNode.data.getArea())
			aNode.left = toDelete(type, aNode.left,value);
		else if(value > aNode.data.getArea())
			aNode.right = toDelete(type, aNode.right,value);
		else if(value == aNode.data.getArea() && type.equalsIgnoreCase(aNode.data.getType())){ //matches area 
			if(aNode.right == null){
				return aNode.left;
			}else if(aNode.left == null){
				return aNode.right;
			}//node has two children 
			Node tmp = aNode;
			aNode = minValue(aNode.right);
			aNode.right = deleteMin(tmp.right);
			aNode.left = tmp.left; 
		}
		return aNode; 
	}
	//find min area in tree
	private Node minValue(Node aNode){
		if(aNode == null) return null;
		else if(aNode.left == null) return aNode;
		else return minValue(aNode.left); 
	}
	//delete min area in tree 
	private Node deleteMin(Node aNode){
		if(aNode == null) return null;
		else if(aNode.left == null) return aNode.right;
		else return aNode.left = deleteMin(aNode.left);
	}
	/*******************************************************************************
	 * given this tree: 
	 * 				     20
	 * 			________/  \________		
	 * 	       /                    \
	 * 	      13					154
	 *       /  \					/  \
	 *      3    15                50  201
	 * 		    /  \              /  \
	 *         14  18            25  40
	 * 
	 * when traversing with the value, lets say 25 we need to
	 * compare aNode.data.getArea() to value (25) 
	 * if(aNode.data.getArea() < value) 
	 * 		this means move right, data not big enough
	 * if(aNode.data.getArea() == value) 
	 * 		this means its right child could be > value but never left child  
	 * if(aNode.data.getArea() > value) 
	 * 		all right children are also greater so delete right subtree
	 * 		but left could also be greater so check left subtree
	 *  
	 ******************************************************************************/
	//delete areas strictly greater than given value 
	public void deleteGreater(double area){
		toDeleteGreater(root, area);
	}
	private void toDeleteGreater(Node aNode, double value){
		if(aNode == null) return;
		if(aNode.left != null)
			toDeleteGreater(aNode.left,value); 
		if (aNode.right != null) //found greater than value check left to see if also greater and delete right subtree
			toDeleteGreater(aNode.right,value);
		if(aNode.data.getArea() > value) //call delete method  
			delete(aNode.getData().getType(), aNode.data.getArea());
		return; 
	}
	/*********************************************************
	 * find maximum value in tree by traversing 
	 * right until null, then print the node before the null 
	 **********************************************************/
	public Node findMax(){
		return maxValue(root);
	}
	//find the max value in the tree
	private Node maxValue(Node aNode){
		Node curr = aNode; 
		while(curr.right != null){ //keep moving right until null 
			curr = curr.right; //set back to root 
		}
		return (curr); //once null return the prev node 
	}
	/************************************************************************
	 * print methods 
	 * print list calls just one print method
	 * single method printAll() to print all (pre, in, post) for simplicity 
	 ************************************************************************/
	public void printList(){
		System.out.println("Printing Tree:\n");
		printInOrder(root);
	}
	public void printAll(){
		System.out.println("  (Pre Order) ------------------------------------------------------------------");
		printPreOrder(root);
		System.out.println("  (In Order)  ------------------------------------------------------------------");
		printInOrder(root);
		System.out.println(" (Post Order) ------------------------------------------------------------------");
		printPostOrder(root);
	}
	// print pre-order, print, explore left, explore right 
	private void printPreOrder(Node aNode){
		if(aNode == null) return;
		aNode.data.print(); //call print method in nodes data 
		printPreOrder(aNode.left);
		printPreOrder(aNode.right);
	}
	//print in-order, explore left, print, explore right
	private void printInOrder(Node aNode){
		if(aNode == null) return;
		printInOrder(aNode.left);
		aNode.data.print(); //print method in data
		printInOrder(aNode.right);
	}
	//print post-order explore left, explore right, print 
	private void printPostOrder(Node aNode){
		if(aNode == null) return;
		printPostOrder(aNode.left);
		printPostOrder(aNode.right);
		aNode.data.print(); //print 
	}
}
