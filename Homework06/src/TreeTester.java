import java.util.Scanner;

/**
 * 
 */

/**
 * @author Logan
 *
 */
public class TreeTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//construct new tree 
		BinaryTreeShape tree = new BinaryTreeShape();
		Scanner kybd = new Scanner(System.in); 
		
		//take input 
		System.out.println("Enter file name"); //prompt for input file name
		String fileName = kybd.nextLine();
		System.out.println("Reading from file: " + "\""+fileName+"\"");
		tree.fileIn(fileName);  //TODO: change to fileName after completed  
		
		//testing all 3 print traversals 
		System.out.println("\n 1)All Print Methods:\n");
		tree.printAll();
		
		//testing delete method 
		System.out.println("\n 2)The Largest Area In The Tree Is: "+ tree.findMax().getData().getArea());
		System.out.println("\n 3)Deleting A Rectangle With Area = 14.0:\n");
		tree.delete("Rectangle",14.0);
		tree.printList();
		
		//deleting all instances greater than a value 
		System.out.println("Enter value to delete"); 
		double delete = kybd.nextDouble();
		System.out.println("\n 4)Deleting All Shapes With Area Greater Than: \"" + delete + "\" (Non Inclusive)\n");
		tree.deleteGreater(delete);
		tree.printList();
	} 

}
