/**
 * 
 */

/**
 * @author Logan
 *
 */
public class RightTriangle implements Shape {

	public double height,base;
	
	public RightTriangle(){
		this.height = 0.0;
		this.base = 0.0; 
	}
	public RightTriangle(double height,double base){
		this.setHeight(height);
		this.setBase(base);
	}
	public double getHeight() {
		return height;
	}	public void setHeight(double height) {
		if (height > 0)
			this.height = height;
		else 
			System.out.println("Error in setting height for Right Triangle");
	}
	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		if(base > 0)
			this.base = base;
		else 
			System.out.println("Error in setting base for Right Triangle");
	}
	public double getArea(){
		return (this.getBase()*this.getHeight())/2.0; // (1/2)*b*h
	}
	public double getPerimeter(){
		return Math.round(this.getBase()+this.getHeight()+(Math.pow(Math.pow(this.getBase(), 2)+Math.pow(this.getHeight(), 2), 0.5))); //math to do the hypo + sideone + sidetwo
	}
	public String getType(){
		return "Right Triangle";
	}
	public void print(){
		System.out.println("Right Triangle:\tHeight: "+this.getBase()+"\tHeight: "+this.getHeight()+"\tArea: "+this.getArea()+"\tPerimeter: "+this.getPerimeter());
	}
}
