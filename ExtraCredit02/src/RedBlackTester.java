/**
 * 
 */

/**
 * @author Logan
 *
 */
public class RedBlackTester  {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RedBlackTree<Integer> testTree = new RedBlackTree<Integer>();
		System.out.println("Int RedBlack Tester!");
		System.out.println("Creating tree");
		System.out.println("Populating Tree with values");
		int[] valArr = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
		for(int i : valArr){
			testTree.insert(i);
		}
		System.out.println("Print: Breadth Order");
		testTree.print();
		
	}
}
