/**
 * 
 */
import java.util.*;

/**
 * @author Logan
 *
 */
public class Graph {
	private class Vertex{ //vertex 
		String name;
		//add edges
		ArrayList<Edge> neighbors;
		public Vertex(String name) {
			this.name = name;
			this.neighbors = new ArrayList<Edge>();
		}
	}
	private class Edge{ //edges 
		Vertex v1;
		double weight;
		public Edge(Vertex v, double weight) {
			this.v1 = v;
			this.weight = weight;
		}
	}
	
	//variables 
	private Vertex origin;
	private ArrayList<Vertex> vertices;
	private ArrayList<Vertex> markedVerticies;
	private ArrayList<Vertex> visitedVerticies;
	private double maxLength; 

	public Graph(double lenght) { //default constructor 
		this.origin = null;
		this.vertices = new ArrayList<Vertex>();
		this.markedVerticies = new ArrayList<Vertex>();
		this.visitedVerticies = new ArrayList<Vertex>();
		this.maxLength = lenght;
	}
	
	public void addVertex(String name){
		if(VertexHasSameName(name)) return; //check for duplicate name 
		Vertex v = new Vertex(name);
		vertices.add(v);
		if(origin == null) origin = v;
	}
	public boolean VertexHasSameName(String name){ //if same name return false 
		for(Vertex vert : vertices){
			if(vert.name.equals(name)) return true;
		}return false; 
	}
	public void addEdge(String from, String to, double weight){
		Vertex v1 = getVertex(from);
		Vertex v2 = getVertex(to); 
		if(v1 == null || v2 == null) return;
		v1.neighbors.add(new Edge(v2,weight));	
	}
	public Vertex getVertex (String name){ //helps with traversals 
		for(Vertex vert : vertices){
			if(vert.name.equals(name)) return vert;
		}return null;
	}
	public void printDFS(){ //depth first search 
		markedVerticies.clear(); //need to clear !! 
		//recursive  depth first search 
		toPrintDFS(origin); //start at origin (like the root) 
	}
	private void toPrintDFS(Vertex v){ //recursive call to traverse through all vertex 
		if(markedVerticies.contains(v)) return; //need to back up 
		System.out.println(v.name);
		markedVerticies.add(v);
		for(Edge e : v.neighbors){
			toPrintDFS(e.v1);
		}
	}
	public void printBFS(){ //breadth first search, searches the neighbors then traverses 
		markedVerticies.clear(); //need to clear !! 
		visitedVerticies.clear();
		System.out.println(origin.name);
		//recursive breadth first search 
		toPrintBFS(origin); //start at origin (like the root) 
	}
	private void toPrintBFS(Vertex v){
		if(markedVerticies.contains(v)) return; //go back up 
		markedVerticies.add(v);
		//visit neighbors 
		for(Edge e : v.neighbors){
			if(visitedVerticies.contains(e.v1) || markedVerticies.contains(e.v1))continue;
			System.out.println(e.v1.name);
			visitedVerticies.add(e.v1);
		}
		//goes down a level 
		for(Edge e : v.neighbors){
			toPrintBFS(e.v1);
		}
	}
	public void printLazyDFS() {
		markedVerticies.clear(); //need to clear !! 
		//recursive lazy DFS
		toPrintLazyDFS(origin); //start at origin (like the root) 		
	}
	private void toPrintLazyDFS(Vertex v){
		if(markedVerticies.contains(v)) return; //need to back up 
		System.out.println(v.name); //print to console 
		markedVerticies.add(v); 
		for(Edge e : v.neighbors){
			if(e.weight < maxLength){ //checks weights against the length 
				toPrintLazyDFS(e.v1); //recursive call if met 
			}else break; //if not break from loop 
		}
	}
	

}
