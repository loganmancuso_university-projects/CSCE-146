/**
 * 
 */

/**
 * @author Logan
 *
 */
public class F_WordHeap {

	private int lastInd;
	private String heap[];

	// param
	public F_WordHeap(int size) {
		this.heap = new String[size];
		this.lastInd = 0;
	}

	// takes the index and looks for that index in array and counts number of f's in the word
	private int numberOf_F(int index) {
		int count = 0;
		for (int i = 0; i < heap[index].length(); i++) {
			char letter = heap[index].toLowerCase().charAt(i);
			if (letter == 'f') {
				count++;
			}
		}
		return count;
	}

	// insert to heap
	public void insert(String fWord) {
		if (lastInd >= heap.length) {
			return;
		}
		heap[lastInd] = fWord;
		bubble();
		lastInd++;
	}

	private void bubble() { //after inserting move to correct position 
		int index = this.lastInd;
		while (index > 0) {
			int parent = index % 2 != 0 ? (index - 1) / 2 : (index - 2) / 2;
			if (parent >= 0 && (numberOf_F(index) - numberOf_F(parent) < 0)) {
				// swap values
				String tmp = heap[parent];
				heap[parent] = heap[index];
				heap[index] = tmp;
			} else {
				break;
			}
			index = parent; // shift index
		}
	}

	public String peek() {
		if (heap == null) {
			System.out.println("cannot peek, heap empty");
			return null;
		}
		return heap[0];
	}

	// remove
	public String delete() {
		String valToRet = peek();
		if (lastInd > 0) {
			heap[0] = heap[lastInd - 1];
			heap[lastInd - 1] = null;
			lastInd--;
			bubbleDown();
		}
		return valToRet;
	}

	private void bubbleDown() { //after removing move to correct position 
		int index = 0;
		while (index * 2 + 1 < lastInd) {
			int smallIndex = index * 2 + 1;
			int largerIndex = index * 2 + 2;
			if (largerIndex < lastInd && numberOf_F(smallIndex) - numberOf_F(largerIndex) > 0) {
				smallIndex = index * 2 + 2;
			}
			if (numberOf_F(index) - numberOf_F(smallIndex) > 0) {
				String tmp = heap[index];
				heap[index] = heap[smallIndex];
				heap[smallIndex] = tmp;
			} else {
				break;
			}
			index = smallIndex;
		}
	}

	// heap sort
	public void heapSort() {
		F_WordHeap tmp = new F_WordHeap(heap.length); //deep copy clone using .clone()
		String deepCopyHeap[] = heap.clone();
		for (int i = 0; i < lastInd; i++) {
			tmp.insert(deepCopyHeap[i]);
		}
		for (int i = lastInd; i >= 0; i--) {
			if (tmp.peek() != null) {
				System.out.println(tmp.delete());
			}
		}
		System.out.println("\n");
	}

	// print Breadth
	public void printBreadth() {
		for (String word : heap) {
			if (word != null) {
				System.out.println(word.toString());
			} else {
				break;
			}
		}
	}
}
