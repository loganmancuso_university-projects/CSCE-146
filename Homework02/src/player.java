//logan mancuso 
public class player {

	//instance variable
	public String name;
	public int wins;
	public int losses;
	public int ties;
	
	//defualt
	public player(){
		this.name = "no name set";
		this.wins = 0;
		this.losses = 0;
		this.ties = 0;
	}
	//parameterized
	public player(String name, int wins, int losses, int ties){
		this.setName(name);
		this.setWins(wins);
		this.setLosses(losses);
		this.setTies(ties);
	}

	//getter and setters
	
	public String getName() {
		return name; 
	}

	public void setName(String name) {
		this.name = name; //allowing any name that is string to be set
	}

	public int getWins() {
		return wins;
	}

	public void setWins(int wins) {
		if (wins >= 0){
			this.wins = wins; 
		}
		else{ //error check
			System.out.println("Error in wins for player "+this.getName()+"Win less than 0");
		}
	}

	public int getLosses() {
		return losses;
	}

	public void setLosses(int losses) {
		if(losses >= 0){
			this.losses = losses;
		}
		else{ //error check
			System.out.println("Error in losses for player "+this.getName()+"loss less than 0");
		}
	}

	public int getTies() {
		return ties;
	}

	public void setTies(int ties) {
		if (ties >= 0){
			this.ties = ties;
		}
		else{ //error check
			System.out.println("Error in ties for player "+this.getName()+"ties less than 0");
		}
	}
}
