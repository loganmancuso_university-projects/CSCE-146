/*
 * logan mancuso
 */

import java.util.Scanner;

public class rpsGame {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner kybd = new Scanner(System.in);
		rpsDatabase rpsDB = new rpsDatabase(); 
		boolean playAgain = false;
		
		//loop for game rematch iff playAgain set to true
		do{
			System.out.println("Welcome to the Rock Paper Scissor Double Round Robbin Tournament");
			System.out.println("Enter number of players:");
			int size = kybd.nextInt();
			kybd.nextLine();
			//enter names of the players and add to the linked list
			for (int i=0; i<size; i++){
				System.out.println("Enter a players name:");
				String name = kybd.nextLine();
				rpsDB.enqueue(name); //call enqueue method pass in users name
			}
			
			//TODO: remove after error checking, list only holding last inputed name
			System.out.println("These are the players:");
			rpsDB.showQueue();
			
			//while loop to control double round robin tournament 
			player firstPlayer = rpsDB.setCurrent(); 
			player currPlayer = rpsDB.setCurrent();
			while(firstPlayer == rpsDB.setCurrent()){
				while(rpsDB.setCurrent().equals(currPlayer)==false){
					//play rps game 
					System.out.println("Please input your choice player: "
										+firstPlayer.getName()+ 
										"\nrock(1), paper(2), scissors(3)");
					int p1Choice = kybd.nextInt();
					System.out.println("Please input your choice player: "
										+currPlayer.getName()+
										"\nrock(1), paper(2), scissors(3)");
					int p2Choice = kybd.nextInt();
					//cases for winning, loosing and ties
					switch(p1Choice){
						case 1:
							if(p1Choice == p2Choice){
								System.out.println("Tie");
								int ties = 1;
								firstPlayer.setTies(ties);
								currPlayer.setTies(ties);
							}else if(p2Choice == 3){ //first player looses
								System.out.println(currPlayer.getName()+"Wins");
								int wins = 1;
								int losses = 1;
								currPlayer.setWins(wins);
								firstPlayer.setLosses(losses);
							}else if(p2Choice == 2){ //first player wins
								System.out.println(firstPlayer.getName()+"Wins");
								int wins = 1;
								int losses = 1;
								currPlayer.setLosses(losses);
								firstPlayer.setWins(wins);
							}
							break;
						case 2:
							if(p1Choice == p2Choice){
								System.out.println("Tie");
								int ties = 1;
								firstPlayer.setTies(ties);
								currPlayer.setTies(ties);
							}else if(p2Choice == 1){ //first player looses
								System.out.println(currPlayer.getName()+"Wins");
								int wins = 1;
								int losses = 1;
								currPlayer.setWins(wins);
								firstPlayer.setLosses(losses);
							}else if(p2Choice == 3){ //first player wins
								System.out.println(firstPlayer.getName()+"Wins");
								int wins = 1;
								int losses = 1;
								currPlayer.setLosses(losses);
								firstPlayer.setWins(wins);
							}
							break;
						case 3:
							if(p1Choice == p2Choice){
								System.out.println("Tie");
								int ties = 1;
								firstPlayer.setTies(ties);
								currPlayer.setTies(ties);
							}else if(p2Choice == 2){ //first player looses
								System.out.println(currPlayer.getName()+"Wins");
								int wins = 1;
								int losses = 1;
								currPlayer.setWins(wins);
								firstPlayer.setLosses(losses);
							}else if(p2Choice == 1){ //first player wins
								System.out.println(firstPlayer.getName()+"Wins");
								int wins = 1;
								int losses = 1;
								currPlayer.setLosses(losses);
								firstPlayer.setWins(wins);
							}
							break;
						default:
							System.out.println("Invalid Coice");
							break;
					}
					//assign points
					rpsDB.next();
				}
				rpsDB.next();
				
			}//end of current game 
			System.out.println("!Game Over!\n"
					+ "Here are the results");
			rpsDB.showQueue(); //print results
			System.out.println("Do you want to play again? (0=yes,1=no)");
			int again = kybd.nextInt();
			if(again == 0){ //play again set true
				System.out.println("!A rematch has been declared!\n"
						+ "Starting new game...");
				playAgain=true;
			}else if(again == 1){ //stop program set false
				System.out.println("Bye");
				playAgain=false; //redundant but prevents error if value is lost 
			}else{ //user input invalid quit
				System.out.println("Invalid, quitting");
				playAgain=false;
			}
			
		}while(playAgain == true); //condition for rematch 
		
	}
}
