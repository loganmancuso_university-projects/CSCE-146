/**
 * 
 */

/**
 * @author Logan
 *
 */
public class Stack <t>{

	public static final int DEF_SIZE = 100;
	public t[] stack;
	int headIndex = 0;
	//defualt
	public Stack(){
		stack = (t[]) new Object[DEF_SIZE];
		int headIndex = 0;
	}
	//param
	public Stack (int size){
		if (size>0){ //construct iff size>&!=0
			stack = (t[]) new Object[size];
		}else{
			System.out.println("Invalid Size<=0");
		}
	}
	/*
	 * Other methods for stack: order (FILO)
	 */
	
	//add
	public void push(t data){
		if (headIndex+1 >= stack.length){ //if full
			System.out.println("Stack Full");
		}else if(stack[0] == null){ //if not full, set to data
			stack[0] = data; 
			return;
		}
		headIndex ++;
		stack[headIndex] = data;
	}
	//remove 
	public t pop(){
		t poped = stack[headIndex]; //set data to last in stack 
		stack[headIndex] = null;
		headIndex --;
		if(headIndex < 0){
			headIndex = 0;
		}
		return poped;
	}
	/**
	 * @return
	 */
	public boolean isEmpty(Stack headIndex) {
		// TODO Auto-generated method stub
		if (headIndex == null){//if head is null then it is empty
			return true;
		}else{
			return false;
		}
	}
	public void showStack(){
		for (int i=0; i<stack.length; i++){
			if (stack[i]!=null){
				System.out.print(stack[i]+" ");
			}
		}
	}
}
