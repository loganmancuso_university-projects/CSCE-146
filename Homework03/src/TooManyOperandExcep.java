/**
 * 
 */

/**
 * @author Logan
 *
 */
public class TooManyOperandExcep extends Exception {
	//default
	public TooManyOperandExcep(){
		super("Too many operands in the equation");
	}
	//param for err msg
	public TooManyOperandExcep(String aMsg){
		super(aMsg);
	}
}
