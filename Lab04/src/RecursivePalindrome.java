/**
 * 
 */
import java.util.Scanner;
/**
 * @author Logan
 *
 */
public class RecursivePalindrome {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner kybd = new Scanner(System.in);
		System.out.println("Welcome to the Palindrome Detector!"
				+ "\nEnter a word/phrase and I will tell if it is a Palindrome"
				+ "\n(or 9 to quit)");
		String input = kybd.nextLine(); //set value and remove white space
		String theInput = input.toLowerCase().replaceAll(" ", ""); //better than trim to remove spaces in between words
		//making new variable "theInput" saves the original value so it can be printed as is was inputed in lines 30-34
		// input = "saippuakivikauppias"; //test input, longest palindrome 
		if (theInput.equalsIgnoreCase("9")){
			System.out.println("bye");
			System.exit(0); 
		}
		else{  
			
			//calls loop 
			loopPalindrome(theInput.trim());
			if (loopPalindrome(theInput)){
				System.out.println("\""+input+"\" is a Palindrome");
			}else{
				System.out.println("\""+input+"\" is not a Palindrome");
			}
			//calls recursive
			recursivePalindrome(theInput.trim()); //call static method to check condition
			if (recursivePalindrome(theInput)){ //if true returned from boolean
				System.out.println("\""+input+"\" is a Palindrome");
			}else{ //if false returned from boolean 
				System.out.println("\""+input+"\" is not a Palindrome");
			}
		}//end main else 
	}//end main 
	
	
	/*
	 * this method will break the word into substrings to 
	 * check if the first and last letter of each substring is 
	 * the same example
	 * taco cat, apply .trim() is now "tacocat"
	 * start at 0 and last index=lenght()-1 
	 * t=t; true 
	 * call method again returning new sub string of "acoca"
	 * a=a; true 
	 * call method again returning new sub string of "coc"
	 * o=o; true
	 * call method again returning new sub string of "o"
	 * o.length() is = 1 || 0 returns true value 
	 * kicks out of loop returning true value from boolean method  
	 */
	
	public static boolean recursivePalindrome(String palindrome){
		if (palindrome.length() == 1 || palindrome.length() == 0){ //if only one or no characters long then it is a palindrome
			return true;
		}
		if (palindrome.charAt(0) == palindrome.charAt(palindrome.length()-1)){//if first and last same
			return recursivePalindrome(palindrome.substring(1, palindrome.length()-1)); //call recursive method
		}
		return false; 
	}
	
	/*
	 * loop method
	 */
	
	public static boolean loopPalindrome(String palindrome){
		if (palindrome.length() == 1 || palindrome.length() ==0){
			return true;
		}else{
			for (int i=0; i<(palindrome.length()/2) -1; i++){
				if (palindrome.charAt(i) == palindrome.charAt(palindrome.length()-i-1)){
					return true;
				}else {
					return false;
				}
			}return false;
		}
	}//end loop palindrome method
}//end class
