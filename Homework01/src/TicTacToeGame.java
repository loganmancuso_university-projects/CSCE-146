/**
 * this prgm will do a tic tac toe against the computer
 * until the user looses or quits at which point it will be 
 * over and printed to console and a file    
 */

/**
 * @author Logan
 *
 */
import java.util.Scanner;


public class TicTacToeGame {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		boolean Rematch = true, addPoint=false, game = true;
		int score = 0;
		do{
			
			TicTacToeDatabase gameDB = new TicTacToeDatabase();	
			Scanner kybd = new Scanner(System.in);
			System.out.println("Welcome to the Tic Tac Toe Tournimate"
					+ "\nHow long can you survive?");
			System.out.println("CPU is O's	You are X's"
					+ "\nGood Luck");
			gameDB.createBoard();
			gameDB.printBoard();
			//game
			while(gameDB.gameOver(game,addPoint)[1]){
				gameDB.addPlayerMove();
				gameDB.gameOver(game,addPoint);
				if (gameDB.gameOver(game, addPoint)[0]){
					score+=score;
					System.out.println("!!!!"+score);
				}
				if(gameDB.gameOver(game, addPoint)[0]==false){
					System.out.println("!!!!issue");
				}
				gameDB.addCompMove();
				gameDB.gameOver(game,addPoint);
				if (gameDB.gameOver(game,addPoint)[0]){
					score+=score;
					System.out.println("!!!!"+score);
				}
				gameDB.printBoard();
			}
			System.out.println("The tournimate has ended"
					+ "\nEnter your name to record your score"+score);
			String name = kybd.nextLine();
			gameDB.addName(new HighScore(name, score));
			//TODO: add score counter printout from winning condition 	
			gameDB.toConsole();
			System.out.println("Score written to \"highscores.txt\"");
			gameDB.printToFile();
			System.out.println("Would you like to play again?"
					+ "\n Yes = 0 No = 1");
			int again = kybd.nextInt();
			if (again == 0){
				Rematch = true;
			}
			else if(again == 1){
				System.out.println("Bye");
				Rematch = false;
			}
			else{
				System.out.println("Invalid choice"
						+ "\nquitting tournimate");
				Rematch = false;
			}
		}while(Rematch == true);
	}
}

