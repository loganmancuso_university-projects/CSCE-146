/**
 * 
 */

/**
 * @author Logan
 *
 */
public class ArrayMaxHeap <Type extends Comparable<Type>> {

	public static final int DEF_SIZE = 100;
	private Type heap[];
	private int size; //looking at last open element 
	
	public ArrayMaxHeap(){
		heap = (Type[])(new Comparable[DEF_SIZE]);
		size = 0;
	}
	public ArrayMaxHeap(int size){
		if (size > 0){
			heap = (Type[])(new Comparable[size]);
			size = 0;	
		}else{
			System.out.println("error in size input");
			return; 
		}
	}
	//insertion
	public void insert(Type val){
		if(size >= heap.length){
			return;
		}
		heap[size] = val;
		//check heap order
		bubble();
		size ++;
	}
	//ensuring max heap conditions met 
	private void bubble(){
		int index = this.size;
		while(index > 0){
			int parent = index%2!=0?(index-1)/2:(index-2)/2;
			if(parent >=0 && heap[index].compareTo(heap[parent])>0){
				Type temp = heap[parent];
				heap[parent] = heap[index]; 
				heap[index] = temp;
			}else break;
			index = parent; 
		}
	}
	public Type peek(){
		if(heap == null)
			return null;
		return heap[0];
	}
	//delete, can only remove root
	public Type delete(){ 
		Type retVal = peek();
		heap[0] = heap[size-1];
		heap[size-1] = null;
		size--;
		bubbleDown();
		return retVal; 
	}
	private void bubbleDown(){ 
		int index = 0;
		while(index*2+1 < size){
			int bigIndex = index*2+1; //assume left larger 
			if(index*2+2 < size && heap[index*2+1].compareTo(heap[index*2+2])<0){
				bigIndex = index*2+2; //right larger
			}
			if(heap[index].compareTo(heap[bigIndex])<0){
				Type tmp = heap[index];
				heap[index] = heap[bigIndex];
				heap[bigIndex] = tmp; 
			}else break;
			index = bigIndex;
		}
	}
	//breadth order
	public void print(){
		for (Type val : heap){
			if(val != null)
				System.out.println(val.toString());
			else break;
		}
	}
	//heap sort, DUPLICATE heap via DEEP COPY, then pop then re-heapify, continue until null 
	public void heapSort(){
		//deep copy
		ArrayMaxHeap tempHeap = new ArrayMaxHeap(heap.length);
		Type deepCopyHeap[] = heap.clone();
		for(int i=0; i<size; i++){
			tempHeap.insert(deepCopyHeap[i]);
		}
		//sort to console 
		for(int i=size; i>=0; i--){
			System.out.print(tempHeap.delete().toString()+"\t");
		}
		System.out.print("\n");
	}
	
	
}
