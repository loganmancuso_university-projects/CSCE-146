/**
 * 
 */

/**
 * @author Logan
 *
 */

import java.util.*;
import java.applet.*;
import java.awt.*;

public class RndFrac extends Applet{
	private Image disp;
	private Graphics drwArea;
	public void init(){
		int h = super.getSize().height;
		int w = super.getSize().width;
		disp = createImage(w,h);
		drwArea = disp.getGraphics();
		//call rndFrac 
		rndFrac(0,(h/2),w,(h/2),drwArea);
	}
	public void paint(Graphics g){
		g.drawImage(disp, 0, 0, null);
	}
	public static void rndFrac(int leftX, int leftY, int rightX, int rightY, Graphics drwArea){
		final int STOP = 4, MAX = 12;
		int midX, midY, delta;
		Random r = new Random();
		if ((rightX-leftX) <= STOP){ //stop condition
			drwArea.drawLine(leftX, leftY, rightX, rightY);
		}else{ //recursive condition
			midX = (leftX+rightX)/2;
			midY = (leftY+rightY)/2;
			midY += delta = r.nextInt(MAX);
			//left side
			rndFrac(leftX, leftY, midX, midY, drwArea);
			//right side
			rndFrac(midX, midY, rightX, rightY, drwArea);
		}	
	}//end rndFrac 
}
