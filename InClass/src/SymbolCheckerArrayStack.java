import java.util.Scanner;

/**
 * @import scanner
 */

/**
 * @author Logan
 *
 */
public class SymbolCheckerArrayStack {
	//variables
	public static final int DEFAULT_SIZE = 100; //default array size
	private static String[] list;
	private static int head=0; 
	 //default
	public SymbolCheckerArrayStack(){
		list = new String [DEFAULT_SIZE]; //initialize array of size DEFAULT_SIZE
		this.head = 0;
	}
	//add
	public static void add(String data){
		if (head+1 >= list.length){ //if full
			System.out.println("List Full");
		}else if(list[0] == null){ //if not full, set to data
			list[0] = data; 
			return;
		}
		head ++;
		list[head] = data;
	}
	//remove 
	public static String remove(int i){
		String removed = list[i]; //set data to last in stack 
		list[i] = null;
		return removed;
	}
	/**
	 * @return a boolean true/false 
	 */
	public static boolean isEmpty() {
		// TODO Auto-generated method stub
		if (list[head] == null){//if head is null then it is empty
			return true;
		}else{
			return false;
		}
	}
	//find match
	public static int findMatch(String val){
		for (int i=0; i<list.length;i++){
			if(val.trim().equalsIgnoreCase(list[i])){
				return i;
			}
		}
		return -1;
	}
	//switch case support 
	public static void supportSwitchCase(String val,String fromArray){
		if((isEmpty()==false) && (findMatch(val)>=0)){
			System.out.println("Found Match "+fromArray+" = "+(list[findMatch(val)]));	
			remove(findMatch(val));
			if(list[0]!=null){
				System.out.println("The List is: ");
				showArray();
			}else{
				System.out.println("The List is Empty: ");
			}
		}else{
			System.out.println("Match Not Found");
			add(fromArray);
		}
	}
	//display the stack 
	public static void showArray(){
		for (String withinList: list){
			if(withinList!=null){
				System.out.print(withinList);
			}
		}
		System.out.println();
	}
	/*
	 * main method 
	 */
	public static void main (String[] args){
		SymbolCheckerArrayStack newSymbolChecker = new SymbolCheckerArrayStack(); //call default 
		Scanner kybd = new Scanner(System.in); 
		System.out.println("Enter String to Analyze"); //prompt user
		String cont = kybd.nextLine(); //take in string 
		for(int i=0; i<cont.length(); i++){ //iterate through the string to find special characters "[,{,(,<"
			System.out.println("Value being analyzed: "+ cont.charAt(i)); 
			if (cont.charAt(i)=='{' 
					||cont.charAt(i)=='['
					||cont.charAt(i)=='('
					||cont.charAt(i)=='<'
					||cont.charAt(i)=='}'
					||cont.charAt(i)==']'
					||cont.charAt(i)==')'
					||cont.charAt(i)=='>')
			{ //if they are one of the symbols do work else skip 
				String tmp = String.valueOf(cont.charAt(i));
				switch (tmp){
					case "{": 
						supportSwitchCase("}",tmp);
						break;
					case "[":
						supportSwitchCase("]",tmp);
						break;
					case "(":
						supportSwitchCase(")",tmp);
						break;
					case "<":
						supportSwitchCase(">",tmp);
						break;
					case "}": 
						supportSwitchCase("{",tmp);
						break;
					case "]":
						supportSwitchCase("[",tmp);
						break;
					case ")":
						supportSwitchCase("(",tmp);
						break;
					case ">":
						supportSwitchCase("<",tmp);
						break;
					default:
						System.out.println("error");
						break;
				}//end switch case 
			}//end if statement
		}//end for loop 
		//if head is null then all openings had a closing and statement is correct	
		if(isEmpty()==true){
			showArray();
			System.out.println("!True!");	
		}else{ //iff head != null then an opening did not have a closing
			showArray();
			System.out.println("!False!");
		}
		System.out.println("Done");
	}//end of main
}//end of class
