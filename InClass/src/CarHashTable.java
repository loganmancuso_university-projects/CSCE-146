/**
 * 
 */
import java.util.*;
/**
 * @author Logan
 *
 */
public class CarHashTable {

	private ArrayList<Car>[] table;
	public CarHashTable(){
		table = new ArrayList[100];
		for(int i=0; i<table.length; i++){
			table[i] = new ArrayList<Car>();
		}
	}
	//hash function 
	private int index(Car aCar){
		int index = 0;
		String makeModel = aCar.getMake()+aCar.getModel();
		for(int i=0; i<makeModel.length(); i++){
			index += makeModel.charAt(i);
		}
		index %= 100;
		return index;
	}
	//inserting
	public void insert(Car aCar){
		int index = index(aCar);
		table[index].add(aCar);
	}
	public void remove(Car aCar){
		int index = index(aCar);
		if(table[index].contains(aCar))
			table[index].remove(aCar);
		else 
			System.out.println("Car not in hash table");
	}
	public void lookUp(Car aCar){
		int index = index(aCar);
		if(table[index].contains(aCar))
			System.out.println(aCar.toString());
		else 
			System.out.println("car not in hash table");
	}
	
}
