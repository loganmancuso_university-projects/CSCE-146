/**
 * 
 */

/**
 * @author Logan
 *
 */
import java.util.*;
public class SortAlg {
	//bubble sort
	public static int[] bubbleSort(int a[]){
		boolean swapped = false;
		int n = a.length;
		do{
			swapped = true;
			for(int i=0; i<n-1; i++){
				if(a[i]>a[i+1]){
					int temp = a[i];
					a[i] = a[i+1];
					a[i+1] = temp;
					swapped=false;
				}
			}
		}while(swapped==false);
		return a;
	} 
	//recursive Selection sort
	public static int[] recurSelectionSort(int array[], int i){
		int minIndex, tmp;
		if(i==array.length-1){ //stopping condition
			return array;
		}
		tmp = minIndex = i;
		for(int j=i+1;j<array.length;j++){
			if(array[i] < array[minIndex]){
				minIndex = j;
			}
		}
		tmp = array[i];
		array[i] = array[minIndex];
		array[minIndex] = tmp;
		return recurSelectionSort(array,i+1);
	}
	// merge sort 
	public static void mergeSort(int a[]){
		int size = a.length;
		if(size<2){ //Halting condition
			return;
		}
		int middle = size/2, 
			leftSize = middle, 
			rightSize=(size-middle),
			left[] = new int[leftSize],
			right[] = new int[rightSize];
		//populate left 
		for(int i=0; i<middle; i++){
			left[i] = a[i];
		}
		for(int i=middle; i<size; i++){
			right[i-middle] = a[i]; //if wanting to add middle to i 
		}
		//recursive
		mergeSort(left);
		mergeSort(right);
		//merge back
		merge(left,right,a);
	}
	//supporting method for merge 
	public static void merge(int left[], int right[], int a[]){
		int leftSize = left.length,
			rightSize = right.length,
			i=0,//index left
			j=0, //index right
			k=0; //index a
		while(i<leftSize && j<rightSize){
			if(left[i] <= right[j]){
				a[k] = left[i];
				i++; //move left counter up
				k++; //move final a[] up 
			}else{
				a[k] = right[j];
				j++; //move right counter up
				k++; //move final a[] up
			}
		}
		//fill in rest of a []
		while(i<leftSize){ //if left != empty 
			a[k] = left[i];
			i++;
			k++;
		}
		while(j<rightSize){ //if right != empty 
			a[k] = right[j];
			j++;
			k++;
		}
	}
	public static void quicksort(int a[], int left, int right){
		int index = partition(a,left,right); //call pivot method
		if(left<index-1){
			quicksort(a,left,index-1);
		}if(index<right){
			quicksort(a,index+1,right);
		}
	}
	public static int partition(int a[], int left, int right){
		int i = left,
			j=right;
		int pivot = a[(left+right)/2];
		while(i<=j){
			while(a[i]<pivot){
				i++; //correct position move forward
			}while(a[j]>pivot){
				j--; //correct move backward
			}if(i<=j){
				int temp = a[i];
					a[i ]= a[j];
					a[j] = temp;
				i++; j--;
			}
		}return i;
	}
	public static void main(String[] args){
		//main method
	/*	ArrayList<Integer> arrList = new ArrayList<Integer>();
		System.out.println("Array List:");
		for(int i=0; i<10; i++){
			arrList.add(i);
		}
		for(int i : arrList){
			System.out.println(i);
		}
		System.out.println("Random access 4th in list: "+arrList.get(4));
		Queue <Integer> q = new LinkedList<Integer>();
		System.out.println("Queue:");
		for(int i=0;i<10;i++){
			q.add(i);
		}
		while(q.isEmpty()==false){
			System.out.println(q.remove());
		}
		Stack <Integer> s = new Stack <Integer>();
		System.out.println("Stack:");
		for(int i=0;i<10;i++){
			s.push(i);
		}
		while(s.isEmpty()==false){
			System.out.println(s.pop());
		}*/
		Random r = new Random();
		int a[] = new int [1000];
		for(int i=0; i<a.length; i++){
			a[i]=r.nextInt(99);
			//System.out.println(a[i]);
		}
		System.out.println("Merge Sorting");
		mergeSort(a);
		for(int i=0; i<a.length; i++){
			System.out.println(a[i]);
		}
	}
}
