import java.util.Scanner;
import java.util.Random;
public class MatrixProgram {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		Random randomNumber = new Random();
		System.out.println("Welcome to the Matrix \"Doubler\"");
		//loop
		boolean quit = false;
		while(quit == false)
		{
			System.out.println("Press 1 to continue or 9 to quit");
			String temp = input.next();
			if(temp.contains("9"))
			{
				quit = true;
				break;
			}
			
			//populate the matrix
			System.out.println("Enter the Height and Width of the matrix (separated by a space)");
			int height = input.nextInt(); //height for the matrix
			int width = input.nextInt(); //width for the matrix
			if (height <=1 || width <=1) //if user inputs a negative number, zero or 1 system quits 
			{
				System.out.println("Invalid input");
				break;
			}
			int [][] matrix = new int [height][width]; //construct matrix with specified dimensions
			for (int j=0; j<height; j++)
			{
				for (int k=0; k<width; k++)
				{
					//generate and assign random number (0-9) to (j,k) coordinate in matrix
					matrix[j][k] = randomNumber.nextInt(9-0+1)+0;
				}
			}
			
			//print the randomly generated matrix
			System.out.println("\nThe Randomly Generated Matrix is:");
			/*
			 * this is basic printing of a 2D array first loop counts height the 
			 * other counts width, coordinates are printed & tabbed with new line 
			 * at end of the width loop using \n char 
			*/
			for (int i=0; i<height; i++) 
			{
				for (int j=0; j<width; j++)
				{
					System.out.print(matrix[i][j]+"\t");
				}
				System.out.print("\n");
			}
			
			//"Double" the matrix
			System.out.println("\nThe \"Doubled\" Matrix is:");
			for (int i=0; i<height; i++) //counts original height
			{
				for (int g=0; g<2; g++) //forces a reprint of line above(number "2" can be changed to desired numb of reprint lines)
				{
					for (int j=0; j<width; j++)
					{
						//print same index twice
						System.out.print(matrix[i][j]+"\t"); 
						System.out.print(matrix[i][j]+"\t");
						
						/*or do this after line 65 to have another loop print "q" quantity of that coordinate 
						 * (q can be any number)
						  for(int l=0; l<q; l++)
						{
							System.out.print(matrix[i][j]+"\t"); 
						} 
						 */
					}
					System.out.print("\n");
				}
			}
			
		}
	}

}
