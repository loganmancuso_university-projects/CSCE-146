/**
 * 
 */

/**
 * @author Logan
 *
 */
public class Phobia {
	private String name, description;
	
	public Phobia(){
		this.name = "no name set";
		this.description = "no description set";
	}
	/**
	 * @param name
	 * @param description
	 */
	public Phobia(String name, String description) {
		this.name = name;
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String toString(){
		return this.name+": "+this.description;
	}
	public boolean equals(Phobia phobia){
		return this.name.equals(phobia.getName());
	}

}
