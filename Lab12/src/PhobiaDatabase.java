import java.util.ArrayList;

/**
 * 
 */

/**
 * @author Logan
 *
 */
public class PhobiaDatabase {

	private ArrayList<Phobia>[] table;

	public PhobiaDatabase() { // construct the table
		table = new ArrayList[26];
		for (int i = 0; i < table.length; i++) {
			table[i] = new ArrayList<Phobia>();
		}
	}
	public boolean contains(Phobia aPhobia,int index){
		for(Phobia phobia: table[index]){
			if(phobia.getName().equalsIgnoreCase(aPhobia.getName())
				&&phobia.getDescription().equalsIgnoreCase(aPhobia.getDescription())){
				table[index].remove(phobia);
				return true;
			}
		}
		return false;
	}
	private int index(Phobia aPhobia) { // find the index placement of each phobia
		int index = aPhobia.getName().charAt(0);
		index %= 26;
		return index;
	}

	public void insert(Phobia aPhobia) { // add to the table
		int index = index(aPhobia);
		table[index].add(aPhobia);
	}

	public void remove(Phobia aPhobia) { // remove from the table
		int index = index(aPhobia);
		if (contains(aPhobia,index)) {
			System.out.println("Phobia removed from table");
		}else
			System.out.println("Phobia not in table");
	}

	public void find(String name) { // find the name of a phobia if in table
		Phobia aPhobia = new Phobia(name, "nothing");
		int index = index(aPhobia);
		for(Phobia phobia: table[index]){
			if(phobia.getName().equalsIgnoreCase(name)){
				System.out.println("Phobia found");
				System.out.println("Name: " + name + " \nDefinition: " + phobia.getDescription());
				break;
			}else{
				System.out.println(name + " is not in table");
			}
		}
	}

	public void print() {
		for (int i = 0; i < table.length; i++) { // move down the rows of the table
			for (Phobia aPhobia : table[i]) { // move through the array list in each row
				System.out.println(aPhobia.toString());
			}
		}
	}
}
