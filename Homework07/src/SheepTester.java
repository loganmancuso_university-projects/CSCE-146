import java.util.Scanner;

/**
 * 
 */

/**
 * @author Logan
 *
 */
public class SheepTester {
	public static void main(String[] args) {

		SheepHeap sheepHeap = new SheepHeap();
		Scanner kybd = new Scanner(System.in);
		// take input
		System.out.println("Enter file name"); // prompt for input file name
		String fileName = kybd.nextLine();
		System.out.println("Reading from file: " + "\"" + fileName + "\"");
		sheepHeap.fileIn(fileName); // TODO: change to fileName after completed

		// testing sheep roll call
		System.out.println("\n 1)Printing Sheep\n");
		sheepHeap.sheepRollCall();

		// testing delete method
		System.out.println("\n 2)Deleting 5 Sheep From The Heap\n");
		for (int i = 0; i < 5; i++) {
			System.out.println("\n-------Deletion number: " + (i + 1) + "--------\n");
			sheepHeap.removeSheep();
			sheepHeap.sheepRollCall();
		}
		// heap sort
		System.out.println("\nTesting Heap Sort\n");
		sheepHeap.sheepHeapSort();
	}
}
